#!/bin/bash

PREEMPTIBLE='--preemptible'
TYPE="n1-highcpu-64"
ZONE="us-central1-c"

NUM=100
PREFIX="machine"

ACC=""

for i in $(seq 1 $NUM); do ACC=$ACC$PREFIX$i" "; done

echo $ACC

echo "gcloud compute instances create $ACC $PREEMPTIBLE --zone=$ZONE --machine-type=$TYPE --image-project ubuntu-os-cloud --image-family ubuntu-1604-lts  --metadata-from-file startup-script=startup.sh"

gcloud compute instances create $ACC $PREEMPTIBLE --zone=$ZONE --machine-type=$TYPE --image-project ubuntu-os-cloud --image-family ubuntu-1604-lts  --metadata-from-file startup-script=startup.sh

echo "bash keep.sh $ZONE $ACC"

bash keep.sh $ZONE $ACC
