#!/bin/bash

zone=$1



while true 
do
  echo "*** check status of compute instances and start if not RUNNING ***"
  for host in "${@:2}"; do
    #host=${hosts[$i]}
    #zone=${zones[$i]}
    status=`gcloud compute instances describe $host --zone $zone | grep status`
    echo -n "[`export TZ=JST-9;date +%Y/%m/%d-%H:%M:%S`] $host $status"
    if [ "$status" = "status: RUNNING" ]; then
      echo " => OK"
    else
      if [ "$status" = "status: STOPPING" ]; then
        echo -n " => wait 20sec"
        sleep 20
      fi
      echo " => NG => try to start it"
      gcloud compute instances start $host --zone $zone
      status=`gcloud compute instances describe $host --zone $zone | grep status`
      echo  "[`export TZ=JST-9;date +%Y/%m/%d-%H:%M:%S`] $host $status"
      echo " => OK"
    fi
  done
  sleep 120
done
